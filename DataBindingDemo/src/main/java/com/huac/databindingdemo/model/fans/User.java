package com.huac.databindingdemo.model.fans;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/18/16
 * @Description
 */
public class User {
    private final String firstName;
    private final String lastName;
    private final long birthday;

    public User(String firstName, String lastName,long birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getBirthday() {
        return birthday;
    }
}