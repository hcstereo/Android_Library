package com.huac.databindingdemo.model.star;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.huac.databindingdemo.BR;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/18/16
 * @Description
 */
public class User extends BaseObservable {
    private final String firstName;
    private final String lastName;
    private final long birthday;
    public boolean isVip;

    public User(String firstName, String lastName, long birthday) {
        this.firstName = "用户名:" + firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }
    @Bindable
    public boolean isVip() {
        return isVip;
    }
    @Bindable
    public String getFirstName() {
        return firstName;
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    @Bindable
    public long getBirthday() {
        return birthday;
    }

    public void setIsVip(boolean isVip) {
        this.isVip = isVip;
        notifyPropertyChanged(BR.lastName);

    }

}