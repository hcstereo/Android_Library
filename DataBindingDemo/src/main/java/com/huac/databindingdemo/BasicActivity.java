package com.huac.databindingdemo;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.huac.activeandroid.binding.BasicBinding;
import com.huac.databindingdemo.model.star.User;
import java.util.Date;

public class BasicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        BasicBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_basic);
        User user = new User("Hua","Li",new Date().getTime());
        user.setIsVip(true);
        binding.setUser(user);
    }
}
