package com.huac.databindingdemo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/18/16
 * @Description
 */
public class DateUtil {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日");
    public static String format(long time){
        Date date = new Date(time);
        return simpleDateFormat.format(date);
    }
}
