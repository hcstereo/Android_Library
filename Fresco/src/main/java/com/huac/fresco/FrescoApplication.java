package com.huac.fresco;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/13/15
 * @Description
 */
public class FrescoApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
