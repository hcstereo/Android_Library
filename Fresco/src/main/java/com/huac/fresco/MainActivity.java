package com.huac.fresco;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

public class MainActivity extends AppCompatActivity {

    private SimpleDraweeView simpleDraweeView;
    private SimpleDraweeView simpleDraweeView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        simpleDraweeView = (SimpleDraweeView) findViewById(R.id.my_image_view);
        simpleDraweeView2 = (SimpleDraweeView) findViewById(R.id.image_view);
        Uri uri = Uri.parse("https://raw.githubusercontent.com/facebook/fresco/gh-pages/static/fresco-logo.png");
        Uri uri2 = Uri.parse("http://pic22.nipic.com/20120705/668573_091208280175_2.jpg");
        simpleDraweeView.setImageURI(uri);
        simpleDraweeView2.setImageURI(uri2);
        GenericDraweeHierarchy hierarchy = simpleDraweeView2.getHierarchy();
        RoundingParams roundingParams = hierarchy.getRoundingParams();
        roundingParams.setCornersRadius(10);
        hierarchy.setRoundingParams(roundingParams);
    }
}
