package com.huac.databinding.textview;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.huac.databinding.R;
import com.huac.databinding.databinding.ActivityTvBindingBinding;

public class TVBindingActivity extends AppCompatActivity {
    private TextModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_binding);
        ActivityTvBindingBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_tv_binding);
        model = new TextModel("123456","content....");
        binding.setText(model);
        new ModelThread().start();
    }

    private class ModelThread extends Thread{
        private int count;
        @Override
        public void run() {
            super.run();
            while (true){
                try {
                    sleep(1000);
                    model.setTextContent("text="+count++);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tvbinding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
