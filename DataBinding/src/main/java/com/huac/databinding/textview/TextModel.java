package com.huac.databinding.textview;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.huac.databinding.BR;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/24/15
 * @Description
 */
public class TextModel extends BaseObservable {
    private String textId;
    private String textContent;
    private long textDate;

    public TextModel(String textId, String textContent) {
        this.textId = textId;
        this.textContent = textContent;
        this.textDate = System.currentTimeMillis();
    }
    @Bindable
    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
        notifyPropertyChanged(BR.textId);

    }
    @Bindable
    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
        notifyPropertyChanged(BR.textContent);
    }
    @Bindable
    public long getTextDate() {
        return textDate;
    }

    public void setTextDate(long textDate) {
        this.textDate = textDate;
        notifyPropertyChanged(BR.textDate);
    }
}
