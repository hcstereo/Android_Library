package com.huac.databinding.data;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/12/15
 * @Description
 */
public class Customer {
    private String username;
    private String gender;
    private int age;
    private String phone;

    public Customer(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
