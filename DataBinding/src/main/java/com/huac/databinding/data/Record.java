package com.huac.databinding.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/12/15
 * @Description
 */
public class Record {
    private String recordId;
    private long date;
    private Customer customer;
    private List<Item> items = new ArrayList<Item>();

    public Record(String recordId, Customer customer) {
        this.recordId = recordId;
        this.customer = customer;
        this.date =System.currentTimeMillis();
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void buy(Item item){
        items.add(item);
    }
}
