package com.huac.databinding.data;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/12/15
 * @Description
 */
public class Item {
    private String itemName;
    private String itemId;
    private float price;

    public Item(String itemName) {
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
