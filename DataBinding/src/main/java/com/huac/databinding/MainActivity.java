package com.huac.databinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.huac.databinding.data.Customer;
import com.huac.databinding.data.Record;
import com.huac.databinding.databinding.ActivityMainBinding;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Customer customer = new Customer("Kevin");
        customer.setAge(20);
        binding.setCustomer(customer);
        Record record = new Record(UUID.randomUUID().toString(),customer);
        binding.setRecord(record);


    }
}
