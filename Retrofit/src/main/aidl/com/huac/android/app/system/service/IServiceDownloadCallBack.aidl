// IServiceDownloadCallBack.aidl
package com.huac.android.app.system.service;

// Declare any non-default types here with import statements
/**
 * Example of defining an interface for calling on to a remote service
 * (running in another process).
 */
oneway interface IServiceDownloadCallBack {

    void onUploadingProgress(in String item);

    void onPrepareStartUpload(in String item);

    void onPreparePauseUpload(in String item);

    void onStartUpload(in String item);

    void onPauseUpload(in String item);

    void onDeleteUpload(in String item);

    void onFinishUpload(in String item);

    void onErrorUpload(in int errorCode,in String item);

    void onUpdateOpToken(String opToken);
}
