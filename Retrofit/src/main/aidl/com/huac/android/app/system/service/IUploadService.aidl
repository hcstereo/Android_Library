// IUploadService.aidl
package com.huac.android.app.system.service;

// Declare any non-default types here with import statements
import com.huac.android.app.system.service.IServiceDownloadCallBack;
interface IUploadService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);
    void registerCallBack(IServiceDownloadCallBack callBack);
    void unregisterCallback(IServiceDownloadCallBack callBack);
    int getServiceId();
    void addDownloadTaskSync(in String item);
    void cancelDownloadingTaskSync(in String item);
    void deleteDownloadingTaskSync(in String item);
    void deleteDownloadedTaskSync(in String item);
    void pauseTaskSync(in String item, in boolean startNext);
    List<String> getDownloadingList();
    List<String> getDownloadPenddingList();
    void updateDownloadPenddingList(in String item);
    void cancelAllRemainTasks();
    void restartAllRemainTasks();
    void UserLogOff();
    void UserLogin(in String userId,in String opToken,in boolean needCombine);
    void updateVideoPassSuccess(in String fileId);
}
