package com.huac.android.app.model.download;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.UUID;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description model of downloading file.
 */
@Table(name = "download_file_info")
public class DownloadFileInfo extends Model{
    @Column(name = "fileId")
    public String fileId;
    @Column(name = "fileName")
    public String fileName;
    @Column(name = "fileLength")
    public long fileLength;
    @Column(name = "fileUrl")
    public String fileUrl;
    @Column(name = "hasDownloadTime")
    public long hasDownloadTime;

    public DownloadFileInfo(){
        fileId = UUID.randomUUID().toString();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public long getHasDownloadTime() {
        return hasDownloadTime;
    }

    public void setHasDownloadTime(long hasDownloadTime) {
        this.hasDownloadTime = hasDownloadTime;
    }
}
