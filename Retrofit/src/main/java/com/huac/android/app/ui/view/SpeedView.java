package com.huac.android.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.huac.android.R;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/15/15
 * @Description
 */
public class SpeedView extends RelativeLayout{

    private Context context;
    private LayoutInflater inflate;

    public SpeedView(Context context) {
        super(context);
        initView(context);
    }

    public SpeedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SpeedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        this.context = context;
        inflate = LayoutInflater.from(context);
        inflate.inflate(R.layout.vw_recycler_view, this);
    }

}
