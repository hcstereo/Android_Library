package com.huac.android.app.system.download;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description 下载监听
 */
public interface DownloadTaskListener {
    void onDownloadStartFailed(DownloadTask task);
    void onDownloadStartSuc(DownloadTask task);
    void onDownloadFinish(DownloadTask task);
    void onDownloadError(DownloadTask task);
}
