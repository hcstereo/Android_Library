package com.huac.android.app.http;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.http.Streaming;
import rx.Observable;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/13/15
 * @Description
 */
public interface DownloadService {
    @POST("vars/download")
    @Streaming
    Call<ResponseBody> downloadFile(@Query("download_path") String download_path);
    //Observable<ResponseBody> downloadFile(@Query("download_path") String download_path);
    @POST("vars/download")
    @Streaming
    Observable<ResponseBody> download(@Query("download_path") String download_path);
}
