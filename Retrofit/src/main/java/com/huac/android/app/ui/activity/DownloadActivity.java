package com.huac.android.app.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.huac.android.R;
import com.huac.android.app.model.download.SpeedModel;
import com.huac.android.app.system.ApiDefine;
import com.huac.android.app.system.Constants;
import com.huac.android.app.system.controller.DownloadManager;
import com.huac.android.app.system.download.DownloadTask;
import com.huac.android.app.ui.Adapter.DownLoadAdapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DownloadActivity extends AppCompatActivity {
    private static final String TAG = DownloadActivity.class.getSimpleName();
    @Bind(R.id.download_recycler_view) RecyclerView recyclerView;

    private List<SpeedModel> speedModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter(Constants.DOWNLOAD_ACTION_UPDATE);
        registerReceiver(mReceiver, intentFilter);
        setContentView(R.layout.activity_download);
        ButterKnife.bind(this);
    }

    private LinkedList<DownloadTask> addDownloadTask(){
        LinkedList<DownloadTask> linkedList = new LinkedList<>();

        DownloadTask task1 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao1.apk");
        DownloadTask task2 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao2.apk");
        DownloadTask task3 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao3.apk");
        DownloadTask task4 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao4.apk");
        DownloadTask task5 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao5.apk");
        DownloadTask task6 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao6.apk");
        DownloadTask task7 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao7.apk");
        DownloadTask task8 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao8.apk");
        DownloadTask task9 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao9.apk");
        DownloadTask task10 = new DownloadTask(this, "http://"+ ApiDefine.SERVER_HOST_IP+":8282/vars/download/paopao10.apk");

        linkedList.add(task1);
        linkedList.add(task2);
        linkedList.add(task3);
        linkedList.add(task4);
        linkedList.add(task5);
        linkedList.add(task6);
        linkedList.add(task7);
        linkedList.add(task8);
        linkedList.add(task9);
        linkedList.add(task10);
        return linkedList;
    }

    private void initRecyclerView(LinkedList<DownloadTask> linkedList){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        //recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL));
        for(int i=0;i<linkedList.size();i++){
            DownloadTask task = linkedList.get(i);
            SpeedModel model = new SpeedModel(task.getFileUrl());
            //speedModels.add(model);
            speedModels.add(task.getSpeedModel());
            model.setFileUrl(task.getFileUrl());
        }
        Log.d(TAG,"initRecyclerView() linkedList size is "+linkedList.size());
        Log.d(TAG,"initRecyclerView() speedModels size is "+speedModels.size());
        recyclerView.setAdapter(new DownLoadAdapter(this, speedModels));
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDownload();
    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        DownloadManager.getInstance().saveDownloadTasks();
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.DOWNLOAD_ACTION_UPDATE)) {
                int finished = intent.getIntExtra("finished", 0);
                String fileUrl = intent.getStringExtra("fileUrl");
                int speed = intent.getIntExtra("speed", 0);
                if(finished == 100){
                    Toast.makeText(getApplicationContext(), "下载完成", Toast.LENGTH_LONG).show();
                }
                //更新数据
                speed = (int) (speed * 1f / 1024);
                for(SpeedModel speedModel : speedModels){
                    if(speedModel.getFileUrl().equals(fileUrl)){
                        speedModel.setDownloadSpeed(speed);
                        speedModel.setDownloadPercent(finished);
                    }
                }
            }
        }
    };

    private void initDownload(){

        Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                subscriber.onNext(DownloadManager.getInstance().initDownloadTasks(DownloadActivity.this));
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG,"onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG,"onError");
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(TAG,"onNext");
                        if(!aBoolean){
                            Log.d(TAG,"onNext create new files downloading.");
                            LinkedList<DownloadTask> linkedList = addDownloadTask();
                            initRecyclerView(linkedList);
                            DownloadManager.getInstance().addDownloadTaskAll(linkedList);
                            DownloadManager.getInstance().executeQueue();
                        }else{
                            Log.d(TAG,"onNext execute database downloads.");
                            initRecyclerView(DownloadManager.getInstance().getDownloadTasks());
                            DownloadManager.getInstance().executeQueue();
                        }
                    }
                });
    }
}
