package com.huac.android.app.system;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description
 */
public class RetrofitApplication extends Application{
    private RetrofitApplication mInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        ActiveAndroid.initialize(this);
    }

    public RetrofitApplication getApplication(){
        return mInstance;
    }
}
