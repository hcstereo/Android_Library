package com.huac.android.app.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.huac.android.R;
import com.huac.android.app.http.DownloadService;
import com.huac.android.app.http.UserService;
import com.huac.android.app.model.MUser;
import com.huac.android.app.system.ApiDefine;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView mResponseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResponseText = (TextView) findViewById(R.id.tv_response);
        //listUser();
        getUser("Kevin");
        //downloadFile("paopao.apk");

        //Intent i= new Intent(this, ServiceDownload.class);
        // potentially add data to the intent
        //i.putExtra("KEY1", "Value to be used by the service");
        //startService(i);
    }

    private void listUser(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.107:8282")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UserService service = retrofit.create(UserService.class);
        Call<List<MUser>> call = service.listUser();
        call.enqueue(new Callback<List<MUser>>() {

            @Override
            public void onResponse(Response<List<MUser>> response, Retrofit retrofit) {
                Log.d(TAG, "successful " + response.message());
                List<MUser> users = response.body();
                StringBuilder builder = new StringBuilder();
                for (MUser u : users) {
                    builder.append(u.getUserName() + "," + u.getGender() + "," + u.getAge() + "\n");
                    Log.d(TAG, builder.toString());
                }
                mResponseText.setText(builder.toString());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "failed ");
                mResponseText.setText("failed");
            }
        });
    }

    private void getUser(final String userName){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient httpClient = new OkHttpClient();
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.interceptors().add(logging);  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+ ApiDefine.SERVER_HOST_IP+":8282")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();
        UserService service = retrofit.create(UserService.class);

        service.getUser(userName)
                .subscribeOn(Schedulers.io())
                .map(new Func1<MUser, MUser>() {

                    @Override
                    public MUser call(MUser mUser) {
                        //开启新线程,对数据进行耗时操作
                        //比如对操作数据库,操作文件等.
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return mUser;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MUser>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError()");
                        e.printStackTrace();
                        mResponseText.setText("onError");
                    }

                    @Override
                    public void onNext(MUser args) {
                        Log.d(TAG, "onNext()");
                        //在主线程更新ui
                        mResponseText.setText("user named " + args.getUserName() + " age is " + args.getAge());
                    }
                });
    }
    private OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(30, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);
        return client;
    }
    private void downloadFile(final String fileName){

        Retrofit retrofit = new Retrofit.Builder()
                .client(getClient())
                .baseUrl("http://192.168.81.95:8282")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final DownloadService downloadService = retrofit.create(DownloadService.class);

        Call<ResponseBody> call = downloadService.downloadFile(fileName);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                Log.d(TAG,"onResponse()");
                final ResponseBody responseBody = response.body();
                Observable.create(new Observable.OnSubscribe<String>() {
                            @Override
                            public void call(Subscriber<? super String> subscriber) {
                                //downloadFileProgress(responseBody);
                                File file = new File("/sdcard/ooooo.apk");
                                Log.d(TAG,"contentType is "+responseBody.contentType().toString());
                                try {
                                    FileOutputStream outputStream = new FileOutputStream(file);
                                    InputStream inputStream = responseBody.byteStream();
                                    long totalLength = responseBody.contentLength();
                                    byte[] b = new byte[4096];
                                    int size;
                                    long download = 0;
                                    Log.d(TAG,"totalLength="+totalLength);
                                    while ((size = inputStream.read(b)) != -1) {
                                        int p = (int)(download * 100*1f / totalLength);
                                        subscriber.onNext(String.valueOf(p));
                                        outputStream.write(b, 0, size);
                                        download += size;
                                    }
                                    outputStream.flush();
                                    inputStream.close();
                                    outputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "onCompleted");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d(TAG, "onError");
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(String progress) {
                                Log.d(TAG, "onNext");
                                mResponseText.setText("下载进度="+progress+"");
                            }
                        });
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG,"onFailure()");
            }
        });
    }

}
