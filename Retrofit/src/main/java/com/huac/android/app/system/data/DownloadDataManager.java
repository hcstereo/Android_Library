package com.huac.android.app.system.data;

import android.content.Context;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.huac.android.app.model.download.DownloadFileInfo;
import com.huac.android.app.model.download.ThreadInfo;
import com.huac.android.app.system.download.DownloadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description manage table of download list.
 */
public class DownloadDataManager extends DataBaseManager<ThreadInfo>{
    private static final String TAG = DownloadDataManager.class.getSimpleName();

    private Context context;
    private static DownloadDataManager mInstance;
    private static final int DEFAULT_THREAD_NUMBER = 9;

    private DownloadDataManager(Context context){
        this.context = context;
    }
    public static synchronized DownloadDataManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new DownloadDataManager(context);
        }
        return mInstance;
    }

    @Override
    public ThreadInfo getData(ThreadInfo downloadThreadInfo) {
        return null;
    }

    @Override
    public boolean deleteData(ThreadInfo downloadThreadInfo) {
        return false;
    }

    @Override
    public ThreadInfo modifyData(ThreadInfo downloadThreadInfo) {
        return null;
    }

    @Override
    public ThreadInfo saveData(ThreadInfo downloadThreadInfo) {
        Log.d(TAG,"save threadInfo "+downloadThreadInfo);
        return downloadThreadInfo;
    }

    @Override
    public List<ThreadInfo> listTopN(int number) {
        return null;
    }

    /**
     * list all 9 threads.
     * @return
     */
    public List<ThreadInfo> list(){
        return listTopN(DEFAULT_THREAD_NUMBER);
    }

    public void saveDownloadInfo(List<ThreadInfo> threads, DownloadFileInfo fileInfo){
        Log.d(TAG, "saveDownloadInfo() start save file info.");
        ActiveAndroid.beginTransaction();
        fileInfo.save();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
        Log.d(TAG, "saveDownloadInfo() finish save file info.");

        Log.d(TAG, "saveDownloadInfo() start save thread info.");

        if(threads != null && threads.size() != 0){
            ActiveAndroid.beginTransaction();
            for(ThreadInfo info:threads){
                info.save();
            }
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
        }

        Log.d(TAG, "saveDownloadInfo() finish save thread info.");
    }

    public LinkedList<DownloadTask> initDownloadTasks(Context context){
        Log.d(TAG,"initDownloadTasks() start.");
        Select select = new Select();
        List<ThreadInfo> threadInfoList = select.from(ThreadInfo.class).execute();
        Map<String,List<ThreadInfo>> stringListMap = new HashMap<>();
        for(ThreadInfo threadInfo:threadInfoList){
            String fileUrl = threadInfo.getFileInfo().getFileUrl();
            List<ThreadInfo> list= stringListMap.get(fileUrl);
            if(list == null){
                list = new ArrayList<>();
            }
            list.add(threadInfo);
            stringListMap.put(fileUrl,list);
        }

        List<DownloadFileInfo> fileInfoList = select.from(DownloadFileInfo.class).execute();
        String url;
        for(DownloadFileInfo fileInfo : fileInfoList){
            url = fileInfo.getFileUrl();
            List<ThreadInfo> list = stringListMap.get(fileInfo.getFileUrl());
            if(list == null){
                stringListMap.put(url,null);
            }
        }

        Log.d(TAG,"stringListMap size is "+stringListMap.size());
        LinkedList<DownloadTask> downloadTasks = new LinkedList<>();
        DownloadTask task;
        for(Map.Entry<String,List<ThreadInfo>> entry : stringListMap.entrySet()){
            List<ThreadInfo> threadInfos = entry.getValue();
            if(threadInfos != null){
                task = new DownloadTask(context,threadInfos.get(0).getFileInfo(),threadInfos);
            }else{
                task = new DownloadTask(context,entry.getKey());
            }
            downloadTasks.add(task);
        }

        Log.d(TAG,"downloadTasks size is "+stringListMap.size());
        Log.d(TAG, "initDownloadTasks() finish.");
        return downloadTasks;
    }
}
