package com.huac.android.app.model.download;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.huac.android.BR;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/15/15
 * @Description
 */
public class SpeedModel extends BaseObservable {
    private String fileUrl;
    private String fileId;
    private String fileName;
    private int downloadSpeed;
    private int downloadPercent;

    public SpeedModel(String fileUrl){
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    @Bindable
    public int getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(int downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
        notifyPropertyChanged(BR.downloadSpeed);
    }
    @Bindable
    public int getDownloadPercent() {
        return downloadPercent;
    }

    public void setDownloadPercent(int downloadPercent) {
        this.downloadPercent = downloadPercent;
        notifyPropertyChanged(BR.downloadPercent);
    }
}
