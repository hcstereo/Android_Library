package com.huac.android.app.system.data;

import java.util.List;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description
 */
public abstract class DataBaseManager<T> {
    public abstract T getData(T t);
    public abstract boolean deleteData(T t);
    public abstract T modifyData(T t);
    public abstract T saveData(T t);
    public boolean saveDataList(List<T> ts){
        for(T t:ts){
            T temp = saveData(t);
            if(temp ==null){
                return false;
            }
        }
        return true;
    }
    public abstract List<T> listTopN(int number);
}
