package com.huac.android.app.system.download;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.huac.android.app.model.download.DownloadFileInfo;
import com.huac.android.app.model.download.SpeedModel;
import com.huac.android.app.model.download.ThreadInfo;
import com.huac.android.app.system.ApiDefine;
import com.huac.android.app.system.Constants;
import com.huac.android.app.system.data.DownloadDataManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description This class is to download a file using several threads.
 */
public class DownloadTask {

    private static final String TAG = DownloadTask.class.getSimpleName();

    private long hasDownload;
    private Context context;
    private List<ThreadInfo> threads = null;
    private static final int DEFAULT_THREAD_COUNT = 3;
    //指定该任务需要开启的线程个数
    private int currentThreadCount = DEFAULT_THREAD_COUNT;
    private String fileUrl;
    private DownloadFileInfo fileInfo;
    private boolean isPaused;
    private boolean isStop;
    private DownloadTaskListener listener;
    private TaskStatus taskStatus = TaskStatus.READY;
    private Timer timer = new Timer();
    private List<DownloadThread> downloadThreads = new ArrayList<>();
    private int threadSpeed;
    private SpeedModel speedModel;

    public DownloadTask(Context context,String fileUrl){
        this.context = context;
        this.fileUrl = fileUrl;
        speedModel = new SpeedModel(fileUrl);
        fileInfo = new DownloadFileInfo();
        fileInfo.setFileUrl(fileUrl);
    }

    public DownloadTask(Context context,DownloadFileInfo fileInfo,List<ThreadInfo> threads){
        this.context = context;
        this.fileUrl = fileInfo.getFileUrl();
        speedModel = new SpeedModel(fileUrl);
        this.fileInfo = fileInfo;
        this.threads = threads;
    }

    public DownloadTask(Context context,String fileUrl,int threadCount){
        this(context, fileUrl);
        this.currentThreadCount = threadCount;
    }

    public void start(DownloadTaskListener listener){
        this.listener = listener;
        new InitThreadInfo().start();
    }
    private void startDownload(){
        listener.onDownloadStartSuc(this);
        taskStatus = TaskStatus.RUNNING;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //sendMessageUi(fileInfo,getTotalSpeed());
                checkFinish(fileInfo,threadSpeed);
                threadSpeed = 0;
                if(taskStatus == TaskStatus.ERROR || taskStatus == TaskStatus.FINISH || taskStatus == TaskStatus.STOP){
                    cancel();
                }
            }
        },0,1000);
        if(threads != null && threads.size() != 0){
            for(ThreadInfo thread : threads){
                DownloadThread tt = new DownloadThread(thread);
                tt.start();//开始
            }
        }
    }
    public void pause(){
        isPaused = true;
        taskStatus = TaskStatus.PAUSE;
    }
    public void resume(){
        isPaused = false;
        taskStatus = TaskStatus.RUNNING;
    }
    public void stop(){
        isStop = true;
        taskStatus = TaskStatus.STOP;
    }
    public String getFileUrl(){
        return this.fileUrl;
    }

    public SpeedModel getSpeedModel() {
        return speedModel;
    }

    private void addThread(ThreadInfo threads){
        this.threads.add(threads);
    }
    public TaskStatus getTaskStatus(){
        return this.taskStatus;
    }
    public boolean isRunning(){
        return taskStatus == TaskStatus.RUNNING;
    }

    class InitThreadInfo extends Thread{
        @Override
        public void run() {
            super.run();
            try {
                initFileInfo();
                initThreadInfo();
                startDownload();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * the truly thread to download file.
     */
    class DownloadThread extends Thread{
        ThreadInfo info;
        DownloadThread(ThreadInfo info){
            this.info = info;
        }

        @Override
        public void run() {
            super.run();
            try {
                long start = info.getStartPoint()+info.getFinishPoint();
                URL url = new URL(fileUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStream input;
                conn.setConnectTimeout(30000);
                conn.setRequestProperty("Range", "bytes=" + info.getStartPoint() + "-" + info.getEndPoint());
                RandomAccessFile raf = new RandomAccessFile(info.getFileInfo().getFileName(), "rws");
                raf.seek(start);

                hasDownload+=info.getFinishPoint();
                input = conn.getInputStream();

                if(206 == conn.getResponseCode()){
                    Log.d(TAG, "开始执行断点下载.");

                    byte[] buffer = new byte[4096];
                    int len;
                    while ((len = input.read(buffer)) != -1){
                        //Log.d(TAG,"Thread "+this.currentThread().getId()+" is downloading file len="+len);
                        raf.write(buffer, 0, len);
                        hasDownload+=len;
                        info.setFinishPoint(info.getFinishPoint() + len);// 设置该线程的下载完成度
                        threadSpeed += len;

                        while (isPaused){
                            if(isStop){
                                break;
                            }
                            try {
                                sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            //sendMessageUi(info.getFileInfo(),0);
                            threadSpeed = 0;
                        }
                        if(isStop){
                            saveThreadInfo(info);
                            break;
                        }
                    }
                }
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURLException");
                e.printStackTrace();
                if(listener != null){
                    listener.onDownloadError(DownloadTask.this);
                }
                taskStatus = TaskStatus.ERROR;
            } catch (IOException e) {
                Log.e(TAG, "IOException");
                e.printStackTrace();
                if(listener != null){
                    listener.onDownloadError(DownloadTask.this);
                }
                taskStatus = TaskStatus.ERROR;
            }
        }
    }

    private void cancelTimer(){
        if(taskStatus != TaskStatus.ERROR){
            timer.cancel();
            taskStatus = TaskStatus.ERROR;
        }
    }
    private void saveThreadInfo(ThreadInfo info){
        DownloadDataManager.getInstance(context).saveData(info);
    }

    /**
     * 通过广播通知修改ui
     * @param info
     * @param speed
     * @param isFinish
     */
    private void sendMessageUi(DownloadFileInfo info,int speed,boolean isFinish){
        Log.d(TAG,"sendMessageUi() speed="+speed);
        Log.d(TAG,"sendMessageUi() fileUrl="+getFileUrl());
        Intent intent = new Intent(Constants.DOWNLOAD_ACTION_UPDATE);
        if(isFinish){
            intent.putExtra("finished", 100);
            intent.putExtra("fileUrl", getFileUrl());
            intent.putExtra("speed", speed);
        }else{
            intent.putExtra("finished", (int) (((float) hasDownload / info.getFileLength()) * 100));
            intent.putExtra("fileUrl", getFileUrl());
            intent.putExtra("speed", speed);
        }
        context.sendBroadcast(intent);
    }

    /**
     * 通过data binding 直接更新数据相应的ui会直接被修改
     * @param info
     * @param speed
     * @param isFinish
     */
    private void updateSpeedModel(DownloadFileInfo info,int speed,boolean isFinish){
        if(isFinish){
            speedModel.setDownloadPercent(100);
        }else{
            speedModel.setDownloadPercent((int) (((float) hasDownload / info.getFileLength()) * 100));
        }
        speedModel.setDownloadSpeed((int) (speed * 1f / 1024));
    }

    /**
     * 检查是否已经下载完
     * @param info
     */
    private void checkFinish(DownloadFileInfo info,int speed){
        if(hasDownload >= fileInfo.getFileLength() && hasDownload != 0){
            sendMessageUi(info,100,true);
            if(listener != null){
                listener.onDownloadFinish(DownloadTask.this);
            }
            taskStatus = TaskStatus.FINISH;
        }else{
            sendMessageUi(info, speed, false);
        }
    }

    private void initFileInfo() throws IOException {
        Log.d(TAG,"initFileInfo()");
        URL url = new URL(fileUrl);
        URLConnection uc = url.openConnection();
        File file = new File(ApiDefine.DEFAULT_SAVE_FILE_PATH);
        if(!file.exists()){
            file.mkdirs();
        }
        fileInfo = new DownloadFileInfo();
        fileInfo.setFileUrl(getFileUrl());
        fileInfo.setFileName(ApiDefine.DEFAULT_SAVE_FILE_PATH+fileUrl.substring(fileUrl.lastIndexOf("/"),fileUrl.length()));
        fileInfo.setFileId(UUID.randomUUID().toString());
        fileInfo.setFileLength(uc.getContentLength());
        fileInfo.setFileUrl(fileUrl);
        Log.d(TAG,"fileInfo length is "+fileInfo.getFileLength());
    }

    private boolean initThreadInfo(){
        Log.d(TAG,"initFileInfo()");
        if(fileInfo == null || fileInfo.getFileLength() <= 0){
            Log.d(TAG,"initFileInfo() init failed.");
            return false;
        }
        if(threads == null){
            threads = new ArrayList<>();
            ThreadInfo threadInfo;
            long eachSize = (long) (fileInfo.getFileLength() * 1f / DEFAULT_THREAD_COUNT);
            for(int i=0;i<DEFAULT_THREAD_COUNT;i++){
                threadInfo = new ThreadInfo();
                threadInfo.setStartPoint(i*eachSize);
                threadInfo.setEndPoint((i + 1) * eachSize);
                threadInfo.setFinishPoint(0);
                threadInfo.setThreadId(UUID.randomUUID().toString());
                threadInfo.setFileInfo(fileInfo);
                addThread(threadInfo);
            }
        }
        Log.d(TAG,"initFileInfo() init success.");
        return true;
    }

    public enum TaskStatus{
        READY,
        RUNNING,
        PAUSE,
        STOP,
        FINISH,
        ERROR
    }

    /**
     * 缓存下载数据
     */
    public void saveTask(){
        Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                if(fileInfo == null){
                    fileInfo = new DownloadFileInfo();
                }
                DownloadDataManager.getInstance(context).saveDownloadInfo(threads,fileInfo);
                subscriber.onNext(true);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG,"saveTask() save task onCompleted.");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG,"saveTask() save task onError.");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(TAG,"saveTask() save task onNext.");
                    }
                });
        this.stop();
    }
}
