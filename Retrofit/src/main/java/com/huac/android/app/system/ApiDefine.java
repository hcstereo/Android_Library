package com.huac.android.app.system;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/7/15
 * @Description this class defines some constants about server api.
 */
public class ApiDefine {
    /**
     * server ip
     */
    public static final String SERVER_HOST_IP = "192.168.80.230";
    /**
     * protocol
     */
    public static final String SERVER_PROTOCOL = "http";
    /**
     * port
     */
    public static final int PORT = 8282;
    /**
     * 文件下载路径
     */
    public static final String DOWNLOAD_FILE_PATH = "vars/download/";

    public static final String DEFAULT_SAVE_FILE_PATH = "/sdcard/download_path/";
}
