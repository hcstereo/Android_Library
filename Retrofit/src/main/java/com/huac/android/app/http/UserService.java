package com.huac.android.app.http;

import com.huac.android.app.model.MUser;
import java.util.List;
import retrofit.Call;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface UserService {

    @GET("/vars/list")
    Call<List<MUser>> listUser();

    @GET("/vars/user/get")
    void getUser(@Query("userId") String userId, Callback<MUser> callback);

    @GET("/vars/user/fetchUInfo")
    Observable<MUser> getUser(@Query("user_name") String username);
}

