package com.huac.android.app.ui.Adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.huac.android.BR;
import com.huac.android.R;
import com.huac.android.app.model.download.SpeedModel;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/15/15
 * @Description
 */
public class DownLoadAdapter extends RecyclerView.Adapter<DownLoadAdapter.DownloadHolder>{
    private static final String TAG = DownLoadAdapter.class.getSimpleName();
    private final LayoutInflater layoutInflater;
    private Context context;
    private List<SpeedModel> speedModels;

    public DownLoadAdapter(Context context,List<SpeedModel> speedModels){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.speedModels = speedModels;
    }

    @Override
    public DownloadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DownloadHolder(layoutInflater.inflate(R.layout.vw_recycler_view,parent,false));
    }

    @Override
    public void onBindViewHolder(DownloadHolder holder, int position) {
        //holder.mTextView.setText(titles[position]);Integer.parseInt()
        SpeedModel speed = speedModels.get(position);
        holder.getBinding().setVariable(BR.speed, speed);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        int count = speedModels == null ? 0 : speedModels.size();
        return count;
    }

    @BindingAdapter("bind:progressPercent")
    public static void setProgressPercent(ProgressBar progressBar, int progress) {
        progressBar.setProgress(progress);
    }

    public static class DownloadHolder extends RecyclerView.ViewHolder{
        private ViewDataBinding binding;
        @Bind(R.id.progress_horizontal)ProgressBar progressBar;
        @Bind(R.id.tv_speed)TextView speed;
        DownloadHolder(View view){
            super(view);
            ButterKnife.bind(this, view);
            binding = DataBindingUtil.bind(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("NormalTextViewHolder","onClick--> position = "+getPosition());

                }
            });
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}