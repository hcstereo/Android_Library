package com.huac.android.app.system.controller;

import android.content.Context;
import android.util.Log;

import com.huac.android.app.model.download.ThreadInfo;
import com.huac.android.app.system.data.DownloadDataManager;
import com.huac.android.app.system.download.DownloadTask;
import com.huac.android.app.system.download.DownloadTaskListener;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description a controller downloading files.
 */
public class DownloadManager implements DownloadTaskListener {
    private static final String TAG = DownloadManager.class.getSimpleName();
    private static DownloadManager mInstance = new DownloadManager();
    private static final int MAX_DOWNLOAD_ITEM_COUNT = 3;
    private LinkedList<DownloadTask> downloadTasks = new LinkedList<DownloadTask>();
    private LinkedList<DownloadTask> downloadErrorTasks = new LinkedList<DownloadTask>();
    private int executeTaskCount;

    private DownloadManager(){
    }

    public static synchronized DownloadManager getInstance(){
        return mInstance;
    }

    /**
     * query download list from database.
     * @return list being downloaded.
     */
    public List<ThreadInfo> getDownloadThreadInfo(Context context){
        List<ThreadInfo> list = DownloadDataManager.getInstance(context).list();
        if(list == null || list.size() == 0){
            ThreadInfo threadInfo = new ThreadInfo();
        }else{
            list = initDownThread();
        }
        return list;
    }

    private List<ThreadInfo> initDownThread(){
        return null;
    }

    public synchronized void addDownloadTask(DownloadTask downloadTask){
        if(downloadTask == null){
            throw new NullPointerException("error null downloading task.");
        }
        downloadTasks.addLast(downloadTask);
    }

    public synchronized void addDownloadTaskAll(LinkedList<DownloadTask> tasks){
        if(tasks == null){
            throw new NullPointerException("error null downloading tasks.");
        }
        while(tasks.size() != 0){
            DownloadTask task = tasks.pop();
            downloadTasks.addLast(task);
        }
    }

    /**
     * 初始化下载队列
     */
    public void resumeDownloadQueue(Context context){
        List<ThreadInfo> threads = DownloadDataManager.getInstance(context).list();
    }

    /**
     * 开始下载队列
     */
    public void executeQueue(){
        if(downloadTasks == null || downloadTasks.size() == 0){
            Log.d(TAG,"executeQueue() downloadTasks queue is invalid (null or size is 0)");
            return;
        }
        for(int i = executeTaskCount;i < MAX_DOWNLOAD_ITEM_COUNT && i < downloadTasks.size();i++){
            executeTaskCount++;
            DownloadTask task = downloadTasks.get(i);
            if(task.getTaskStatus() == DownloadTask.TaskStatus.RUNNING){
                continue;
            }
            if(task.getTaskStatus() == DownloadTask.TaskStatus.READY){
                task.start(this);
            }
        }
    }

    @Override
    public void onDownloadStartFailed(DownloadTask task) {

    }

    @Override
    public void onDownloadStartSuc(DownloadTask task) {

    }

    @Override
    public void onDownloadFinish(DownloadTask task) {
        if(downloadTasks.contains(task)){
            downloadTasks.remove(task);
            Log.d(TAG, "download " + task.getFileUrl() + " finish.");
            executeTaskCount--;
            executeQueue();
        }
    }

    @Override
    public void onDownloadError(DownloadTask task) {
        Log.d(TAG, "download " + task.getFileUrl() + " error.");
        downloadTasks.remove(task);
        if(!downloadErrorTasks.contains(task)){
            downloadErrorTasks.add(task);
        }
        //downloadTasks.addLast(task);
        executeTaskCount--;
        executeQueue();
    }

    public void saveDownloadTasks(){
        for(int i=0;i<downloadTasks.size();i++){
            DownloadTask task = downloadTasks.get(i);
            task.saveTask();
        }
        for(int i=0;i<downloadErrorTasks.size();i++){
            DownloadTask task = downloadErrorTasks.get(i);
            task.saveTask();
        }
    }

    public boolean initDownloadTasks(Context context){
        this.downloadTasks = DownloadDataManager.getInstance(context).initDownloadTasks(context);
        return this.downloadTasks != null && this.downloadTasks.size() != 0;
    }

    private boolean hasDownloadTask(){
        return this.downloadTasks != null && this.downloadTasks.size() != 0;
    }

    public LinkedList<DownloadTask> getDownloadTasks(){
        return this.downloadTasks;
    }
}
