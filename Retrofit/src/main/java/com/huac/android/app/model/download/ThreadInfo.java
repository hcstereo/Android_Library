package com.huac.android.app.model.download;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/14/15
 * @Description a thread of download file.
 */
@Table(name = "thread_info")
public class ThreadInfo extends Model{
    @Column(name = "threadId")
    public String threadId;
    @Column(name = "startPoint")
    public long startPoint;
    @Column(name = "endPoint")
    public long endPoint;
    @Column(name = "finishPoint")
    public long finishPoint;
    @Column(name = "fileInfo", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public DownloadFileInfo fileInfo;

    public ThreadInfo(){

    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public long getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(long startPoint) {
        this.startPoint = startPoint;
    }

    public long getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(long endPoint) {
        this.endPoint = endPoint;
    }

    public long getFinishPoint() {
        return finishPoint;
    }

    public void setFinishPoint(long finishPoint) {
        this.finishPoint = finishPoint;
    }

    public DownloadFileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(DownloadFileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }
}
