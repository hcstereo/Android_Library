package com.huac.rxjava;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * @Description first show activity.
 * @author Kevin
 * @date 2015-12-07
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private LinearLayout contentView;
    private TextView tv;
    private StringBuilder stringBuilder = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contentView = (LinearLayout) findViewById(R.id.layout_image_views);
        tv = (TextView) findViewById(R.id.tv_text);
        //showString(new String[]{"log01","log02","log03","log04"});
        //int[] drawable = {R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher};
        //showDrawable(drawable, contentView);
        //getMap(R.drawable.aaa);

        Student[] students = new Student[3];
        Student student1 = new Student();
        student1.stuName = "Jack";
        student1.stuAge = 15;
        Student student2 = new Student();
        student2.stuName = "Kevin";
        student2.stuAge = 43;
        Student student3 = new Student();
        student3.stuName = "Bob";
        student3.stuAge = 35;

        Course[] courses = new Course[3];
        Course course = new Course();
        course.courseName = "English";

        courses[0] = course;
        courses[1] = course;
        courses[2] = course;

        student1.courses = courses;
        student2.courses = courses;
        student3.courses = courses;

        students[0] = student1;
        students[1] = student2;
        students[2] = student3;

        //showStudentName(students);

        //showStudentCourse(students);

        showStudentCourseFlap(students);
    }

    /**
     * 分别打印传入字符串数组的每一项
     * @param strings
     */
    private void showString(String[] strings){
        Observable.from(strings)
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d(TAG, s);
                    }
                });
    }

    private void showDrawable(final int[] drawables, final ViewGroup contentView){

        Observable
                .create(new Observable.OnSubscribe<Drawable>() {
                    @Override
                    public void call(Subscriber<? super Drawable> subscriber) {
                        Log.d(TAG, "call()");
                        Log.d(TAG,"currentTime="+System.currentTimeMillis());
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG,"after sleep currentTime="+System.currentTimeMillis());
                        for (int d : drawables) {
                            Drawable drawable = getTheme().getDrawable(d);
                            subscriber.onNext(drawable);
                        }
                        subscriber.onCompleted();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Drawable>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError()");
                    }

                    @Override
                    public void onNext(Drawable drawable) {
                        Log.d(TAG, "onNext()");
                        ImageView imageView = new ImageView(MainActivity.this);
                        imageView.setImageDrawable(drawable);
                        contentView.addView(imageView);
                    }
                });
    }

    private void getMap(final int drawableId){
        Observable.just(drawableId)
                .map(new Func1<Integer, Bitmap>() {
                    @Override
                    public Bitmap call(Integer s) {
                        Log.d(TAG, "call1()");
                        Drawable drawable = getTheme().getDrawable(drawableId);
                        if (drawable instanceof BitmapDrawable) {
                            Log.d(TAG, "BitmapDrawable");
                            return ((BitmapDrawable) drawable).getBitmap();
                        } else {
                            Log.d(TAG, "not BitmapDrawable");
                            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                                    : Bitmap.Config.RGB_565);
                            Canvas canvas = new Canvas(bitmap);
                            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                            drawable.draw(canvas);
                            return bitmap;
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Bitmap>() {
                    @Override
                    public void call(Bitmap bitmap) {
                        Log.d(TAG, "call2()");
                        Log.d(TAG, "bitmap=" + bitmap);
                        if (bitmap != null) {
                            ImageView imageView = new ImageView(MainActivity.this);
                            imageView.setImageBitmap(bitmap);
                            contentView.addView(imageView);
                        }
                    }
                });
    }

    private void showStudentName(Student[] students){
        Subscriber<String> subscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() {
                tv.setText(stringBuilder.toString());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                stringBuilder.append(s+"\n");
            }
        };
        Observable.from(students)
                .map(new Func1<Student, String>() {
                    @Override
                    public String call(Student student) {
                        return student.stuName;
                    }
                })
                .subscribe(subscriber);
    }

    private void showStudentCourse(Student[] students){
        Observable.from(students)
                .subscribe(new Action1<Student>() {
                    @Override
                    public void call(Student student) {
                        Course[] courses = student.courses;
                        for(Course course:courses){
                            stringBuilder.append("stuName:"+student.stuName+" course:"+course.courseName+"\n");
                        }
                        tv.setText(stringBuilder.toString());
                    }
                });
    }

    private void showStudentCourseFlap(Student[] students){
        Observable.from(students)
                .flatMap(new Func1<Student, Observable<Course>>() {
                    @Override
                    public Observable<Course> call(Student student) {
                        return Observable.from(student.courses);
                    }
                })
                .subscribe(new Subscriber<Course>() {
                    @Override
                    public void onCompleted() {
                        tv.setText(stringBuilder.toString());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Course course) {
                        stringBuilder.append(course.courseName + "\n");
                    }
                });
    }

    class Student{
        String stuName;
        int stuAge;
        Course[] courses;
    }
    class Course{
        String courseName;
    }
}
