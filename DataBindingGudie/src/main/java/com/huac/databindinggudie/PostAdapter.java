package com.huac.databindinggudie;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huac.databindinggudie.databinding.ItemPostBinding;

import java.util.List;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/24/16
 * @Description
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostHolder>{
    private static final String TAG = PostAdapter.class.getSimpleName();
    private List<Post> mPosts;
    private Context mContext;

    public PostAdapter(List<Post> posts, Context context){
        Log.d(TAG,"PostAdapter() list size=" + posts.size());
        this.mPosts = posts;
        this.mContext = context;
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG,"onCreateViewHolder()");
        ItemPostBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),R.layout.item_post,parent,false);
        PostHolder postHolder = new PostHolder(binding);
        return postHolder;
    }

    @Override
    public void onBindViewHolder(PostHolder holder, int position) {
        ItemPostBinding binding = holder.postBinding;
        binding.setPost(new PostViewModel(mContext,mPosts.get(position)));
    }

    @Override
    public int getItemCount() {
        Log.d(TAG,"size="+mPosts.size());
        return mPosts.size();
    }

    public static class PostHolder extends RecyclerView.ViewHolder{
        private ItemPostBinding postBinding;

        public PostHolder(ItemPostBinding postBinding) {
            super(postBinding.cardView);
            this.postBinding = postBinding;
        }
    }
}
