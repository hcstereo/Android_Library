package com.huac.databindinggudie;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/24/16
 * @Description
 */
public class PostViewModel {

    private Context mContext;
    private Post post;
    private String top;
    private String title;
    private String bottom;
    private int pictureId;

    public PostViewModel(Context context, Post post){
        this.post = post;
        top = post.getTop() + "关注了该问题";
        title = post.getPostTitle();
        bottom = post.getCountComment() + "条评论 " + post.getCountAnswer() + "个回答";
        pictureId = post.getPictureId();
        this.mContext = context;
    }

    public String getTop() {
        return top;
    }

    public String getTitle() {
        return title;
    }

    public String getBottom() {
        return bottom;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public View.OnClickListener onClickPicture(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"click the picture",Toast.LENGTH_LONG).show();
            }
        };
    }
}
