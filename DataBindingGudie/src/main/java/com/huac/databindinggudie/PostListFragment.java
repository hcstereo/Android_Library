package com.huac.databindinggudie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/24/16
 * @Description
 */
public class PostListFragment extends Fragment{
    private static final String TAG = PostListFragment.class.getSimpleName();

    private RecyclerView mRecyclerViewPosts;
    private PostAdapter mPostAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView()");
        View contentView = inflater.inflate(R.layout.fragment_post_list,null);
        mRecyclerViewPosts = (RecyclerView) contentView.findViewById(R.id.recycler_view_posts);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        mRecyclerViewPosts.setLayoutManager(linearLayoutManager);
        mPostAdapter = new PostAdapter(fetchPostList(),getActivity());
        mRecyclerViewPosts.setAdapter(mPostAdapter);
        return contentView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreate()");
        super.onCreate(savedInstanceState);
    }

    private List<Post> fetchPostList(){
        List<Post> postList = new ArrayList<>();
        Post pp1 = new Post();
        pp1.setTop("冬笋关注了该问题");
        pp1.setCountAnswer(29);
        pp1.setCountComment(4);
        pp1.setPostTitle("给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么");
        pp1.setPictureId(R.drawable.picture);
        Post pp2 = new Post();
        pp2.setTop("冬笋关注了该问题");
        pp2.setCountAnswer(29);
        pp2.setCountComment(4);
        pp2.setPostTitle("给陌生的外国人发英语邮件要给陌生的外国人发英语邮件要注意些什么注意些什么");
        pp2.setPictureId(R.drawable.picture);
        Post pp3 = new Post();
        pp3.setTop("冬笋关注了该问题");
        pp3.setCountAnswer(29);
        pp3.setCountComment(4);
        pp3.setPostTitle("给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么");
        pp3.setPictureId(R.drawable.picture);
        Post pp4 = new Post();
        pp4.setTop("冬笋关注了该问题");
        pp4.setCountAnswer(29);
        pp4.setCountComment(4);
        pp4.setPostTitle("给陌生的外国人给陌生的外国人发英英语邮件要注意些什么给陌生的外国人发英语邮件要注意些什么发英语邮件要注意些什么");
        pp4.setPictureId(R.drawable.picture);
        postList.add(pp4);
        postList.add(pp4);
        postList.add(pp4);
        postList.add(pp4);
        postList.add(pp3);
        postList.add(pp2);
        postList.add(pp1);
        return postList;
    }
}
