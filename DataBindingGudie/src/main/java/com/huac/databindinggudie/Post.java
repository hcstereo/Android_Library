package com.huac.databindinggudie;

/**
 * @author Kevin
 * @version V1.0
 * @Date 2/24/16
 * @Description
 */
public class Post {
    private String top;
    private String postTitle;
    private int pictureId;
    private int countComment;
    private int countAnswer;

    public String getTop() {
        return top;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public int getPictureId() {
        return pictureId;
    }

    public int getCountComment() {
        return countComment;
    }

    public int getCountAnswer() {
        return countAnswer;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public void setCountComment(int countComment) {
        this.countComment = countComment;
    }

    public void setCountAnswer(int countAnswer) {
        this.countAnswer = countAnswer;
    }
}
