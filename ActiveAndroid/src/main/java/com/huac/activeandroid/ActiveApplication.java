package com.huac.activeandroid;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/15/15
 * @Description
 */
public class ActiveApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
    }
}
