package com.huac.activeandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.huac.activeandroid.model.User;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Select select = new Select();
    private Delete delete = new Delete();
    @Bind(R.id.text_view)
    protected TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        for(int i=0;i<10;i++){
            User user = new User();
            user.createTime = System.currentTimeMillis();
            user.password = "password";
            user.username = "chenhuachong"+i+"@qiyi.com";
            user.save();
        }

        StringBuilder builder = new StringBuilder();
        List<User> users = select.from(User.class).limit(10).execute();
        for(User u : users){
            builder.append(u.username+";"+u.password+";"+u.createTime);
            builder.append("\n");
        }
        Log.d(TAG,builder.toString());
        textView.setText(builder.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
