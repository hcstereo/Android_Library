package com.huac.activeandroid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * @author Kevin
 * @version V1.0
 * @Date 12/16/15
 * @Description
 */
@Table(name = "user",id = "userId")
public class User extends Model{
    @Column(name = "username")
    public String username;
    @Column(name = "password")
    public String password;
    @Column(name = "createTime")
    public long createTime;
}
